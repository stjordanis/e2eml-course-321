"""
The Wikipedia ECG page
(https://en.wikipedia.org/wiki/Electrocardiography) really helps
to make sense out of the data. It gives just enough domain knowledge
to avoid doing anything really dumb here.

If you haven't already, install the Waveform Database
(https://github.com/MIT-LCP/wfdb-python) package for Python:

    python3 -m pip install wfdb

This is a library specifically tailored physiological data
of the format we'll be working with.

I've downloaded the data to a directory called "data" and unzipped it there.
The file "mit-bih-arrhythmia-database-1.0.0/mitdbdir/intro.html"
is as thorough and clear a data description as I've ever seen.
This dataset has been lovingly cared for.
"""
import os
import matplotlib.pyplot as plt
import numpy as np
import wfdb

DATA_DIR = os.path.join("data", "mit-bih-arrhythmia-database-1.0.0") 


def get_signal(pid):
    signal, _ = wfdb.rdsamp(os.path.join(DATA_DIR, str(pid)), channels=[0])
    return signal


def get_ann(pid):
    ann = wfdb.rdann(os.path.join(DATA_DIR, str(pid)), "atr")
    return ann.sample, ann.symbol


def count_labels(ids):
    label_count = {}
    for id in ids:
        label_locs, labels = get_ann(id)
        for label in labels:
            try:
                label_count[label] += 1
            except KeyError:
                label_count[label] = 1

    print(label_count)


def load_waves(include_ids, include_classes):
    """
    Pre-load all the data.
    Segment it out into three bins: training, tuning, and testing.
    For now, treat beats as independent. Don't worry about segmenting
    by patient. (Separating patients into training, tuning, and training
    groups would be the absolute most rigorous approach,
    but for the sake of making the example clear, we're simplifying
    it a little.)
    Stratify sampling to make sure that less common classes are adequately
    represented in all groups.
    Undersample the dominant "normal" class, in order to maintain
    class balance.

    Choosing the window width (beat_window) here is important.
    Too narrow and we may miss important signal.
    Too wide and the important signal will get diluted,
    or worse, we'll get parts of adjacent beats which could conflict.

    Using the same size window for every training example ensures
    a good apples-to-apples comparison between them.
    No re-centering will be necessary.
    Because part of the data preparation was identifying
    the peak location accurately, we can be confident that
    the information of interest is centered around each label.
    """
    beat_window = 500  # in milliseconds
    f_samp = 360  # sampling rate in Hz
    half_sample_window = int(f_samp * beat_window / (2 * 1000))  # in samples

    training_fraction = .6
    tuning_fraction = .2
    testing_fraction = .2

    training_data = []
    tuning_data = []
    testing_data = []

    # Pull all the examples out of all the subjects' records
    examples = []
    for pid in include_ids:
        signal = get_signal(pid)
        label_locs, labels = get_ann(pid)
        for i_label, label_loc in enumerate(label_locs):
            label = labels[i_label]
            if label in include_classes.keys():
                i_start = label_loc - half_sample_window
                i_end = label_loc + half_sample_window
                mlii_sig = signal[i_start: i_end + 1, :].ravel()
                # Check to make sure the full window is supported by the data.
                # At the start and end of the trace, this is not necessarily
                # the case.
                if mlii_sig.size == 2 * half_sample_window + 1:
                    examples.append((
                        mlii_sig[np.newaxis, :],
                        include_classes[label]))
    np.random.shuffle(examples)

    # Populate the data sets, one class at a time.

    # Use the count of the "lbbb" class to decide how many examples of each
    # class to include. It's the largest non-normal class.
    class_count = 0
    for example in examples:
        if example[1] == "lbbb":
            class_count += 1

    n_class_training = int(class_count * training_fraction)
    n_class_tuning = int(class_count * tuning_fraction)
    n_class_testing = int(class_count * testing_fraction)

    for label in include_classes.values():
        class_training_data = []
        class_tuning_data = []
        class_testing_data = []

        for example in examples:
            if example[1] == label:
                roll = np.random.sample()
                if roll < training_fraction:
                    class_training_data.append(example)
                elif roll < training_fraction + tuning_fraction:
                    class_tuning_data.append(example)
                else:
                    class_testing_data.append(example)
        i_training_data = np.random.choice(
            np.arange(len(class_training_data), dtype=np.int),
            size=n_class_training)
        for i_data in i_training_data:
            training_data.append(class_training_data[i_data])

        i_tuning_data = np.random.choice(
            np.arange(len(class_tuning_data), dtype=np.int),
            size=n_class_tuning)
        for i_data in i_tuning_data:
            tuning_data.append(class_tuning_data[i_data])

        i_testing_data = np.random.choice(
            np.arange(len(class_testing_data), dtype=np.int),
            size=n_class_testing)
        for i_data in i_testing_data:
            testing_data.append(class_testing_data[i_data])

    return training_data, tuning_data, testing_data


def data_generator(examples):
    while True:
        i_data = np.random.choice(len(examples))
        yield examples[i_data]


def get_training_data():
    return data_generator(training_data)


def get_tuning_data():
    return data_generator(tuning_data)


def get_testing_data():
    return data_generator(testing_data)


def test():
    for _ in range(10):
        example = next(testing_data)
        plt.figure()
        plt.plot(example[0])
        plt.xlabel(example[1])
        plt.show()


# For now keep it simple and streamlined.
# Our analyses can be extended later to include a wider set of subjects,
# measurements, and categories.
# 
# * Focus on 100-series subjects (not especially selected
#       for pathological conditions).
# * Focus on MLII (modified limb lead II) signal only.
# * Ignore subjects 102, 104 (MLII not available)
# * Ignore subjects 114 (leads reversed)
# * Ignore subjects 112, 115 through 124 (recorded at twice real time)

include_ids = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]

# Class balance matters. We'll shoot for the roughly same number
# of training examples from each class.
# We can do some oversampling of smaller classes, but too much oversampling
# means we're just overfitting to a handful of examples. For now we'll
# limit ourselves to learning 'N', 'V', '/', and 'L'.
# We can always extend it to other classes later.
# count_labels(include_ids)

# Using
# include_ids = [100, 101, 103, 105, 106, 107, 108, 109, 111, 113]
# * 'N': 13,742 instances. Normal
# * 'L':  4,615 instances. Left bundle branch block
# * '/':  2,078 instances. Paced beat
# * 'V':    677 instances. Premature ventricular contraction
include_classes = {"N": "normal", "L": "lbbb", "/": "paced", "V": "pvc"}

training_data, tuning_data, testing_data = load_waves(
    include_ids, include_classes)
