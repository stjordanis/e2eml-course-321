import conv_one_d_pad_viz as conv1d


signal = [
    .8, .5, .9, -.1, .7, .6, 1, .3, .9, .4, -.3, .2, -.1,
    .4, -.2, -.8, -.6, -.9, .1, -.1, -.3, -1, -.8, -.5, -.9,
]
kernel = [.1, .3, .7, .9, 1, .9, .7, .3, .1]
# conv1d.illustrate(signal, kernel, "wa_pad_zeros", pad="constant", value=0)
# conv1d.illustrate(signal, kernel, "wb_pad_const", pad="constant", value=.5)
# conv1d.illustrate(signal, kernel, "wc_pad_mirror", pad="mirror")
# conv1d.illustrate(signal, kernel, "wd_pad_circular", pad="circular")
conv1d.illustrate(signal, kernel, "we_pad_none", pad="none")
