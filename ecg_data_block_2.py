import numpy as np
import ecg_data_loader_2 as dat


class TrainingData(object):
    def __init__(self):
        self.data = dat.get_training_data()

    def __str__(self):
        return "ECG training data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass


class TuningData(object):
    def __init__(self):
        self.data = dat.get_tuning_data()

    def __str__(self):
        return "ECG tuning data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass


class TestingData(object):
    def __init__(self):
        self.data = dat.get_testing_data()

    def __str__(self):
        return "ECG testing data"

    def forward_pass(self, arg):
        return next(self.data)

    def backward_pass(self, arg):
        pass
