import os
import numpy as np
import matplotlib.pyplot as plt
import heartbeat_classifier_mark_1 as trainer


results_dir = os.path.join("reports", "repeatability")
os.makedirs(results_dir, exist_ok=True)

n_iter = 121
bin_size = 1e4

losses = []
loss_histories = []
for i_iter in range(n_iter):
    print(f"  Run {i_iter + 1} of {n_iter}")
    classifier, tuning_loss, loss_history = trainer.train()
    losses.append(tuning_loss)

    binned_history = []
    for i_bin in range(int(len(loss_history) // bin_size)):
        binned_history.append(np.mean(
            loss_history[int(i_bin * bin_size): int((i_bin + 1) * bin_size)]))
    smoothed_history = np.log10(binned_history)
    loss_histories.append(smoothed_history)

losses = np.array(losses)
print("mean", np.mean(losses))
print("var", np.var(losses))
print("stderr", np.sqrt(np.var(losses) / n_iter))
print("min, max", np.min(losses), np.max(losses))

fig_all = plt.figure()
ax_all = fig_all.gca()

for i_iter in range(n_iter):
    ax_all.plot(loss_histories[i_iter], alpha=.3)

# TODO: save results so they can be reloaded and used to craft a custom plot
ax_all.set_title(f"Loss variation in {n_iter} training/testing runs")
ax_all.set_xlabel(f"x{bin_size:,} iterations")
ax_all.set_ylabel("log10(loss)")
fig_all.savefig(os.path.join(results_dir, f"all_runs.png"))

# Group loss histories by median
n_groups = [3, 5, 7, 11]
for n_group in n_groups:
    fig = plt.figure()
    ax = fig.gca()
    median_losses = []
    for i_iter in range(n_iter // n_group):
        group_losses = losses[i_iter * n_group: (i_iter + 1) * n_group]
        median_loss = np.median(group_losses)
        median_losses.append(median_loss)
        i_median = np.where(group_losses == median_loss)[0][0]
        ax.plot(loss_histories[i_iter * n_group + i_median], alpha=.3)
    ax.set_xlabel(f"x{bin_size:,} iterations")
    ax.set_ylabel("log10(loss)")
    ax.set_title(
        f"Loss variation in {n_iter} training/testing runs,"
        + f" groups of {n_group}")
    fig.savefig(os.path.join(results_dir, f"all_runs_{n_group}.png"))
    plt.close(fig)

    print()
    print("group size", n_group)
    print("mean", np.mean(median_losses))
    print("var", np.var(median_losses))
    print("stderr", np.sqrt(np.var(median_losses) / (n_iter / n_groups)))
    print("min, max", np.min(median_losses), np.max(median_losses))
