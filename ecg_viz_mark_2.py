import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator, FixedFormatter


def make_ecg_plots(
    signal,
    result_history,
    label_locs,
    labels,
    labels_by_index,
    patient_id,
    f_samp,
    half_window,
    pt_spacing,
    images_dir,
):
    snippet_length = 3  # seconds
    signal_pts_per_snippet = f_samp * snippet_length
    results_pts_per_snippet = f_samp * snippet_length / pt_spacing
    n_snippets = int(result_history.shape[1] / results_pts_per_snippet)

    label_name = {
        "N": "Normal",
        "L": "LBBB",
        "/": "Paced",
        "V": "PVC",
    }

    # Smooth the results
    # Convolve with a rectangular window in order to calculate a moving
    # average.
    # The phenomena we're interested in (individual heartbeats)
    # usually occur at least .5 seconds (180 time steps) apart.
    # The fluctuations we'd like to remove tend to be
    # .1 seconds (36 time steps) or shorter. A sliding window of
    # .17 seconds (61 time steps) seems like a reasonable middle value.
    # Odd numbers of time steps avoid introducing any weird half-timestep
    # shifts.
    filter_length = 61
    smooth_result_history = np.zeros(result_history.shape)
    window_filter = np.ones(filter_length) / filter_length
    # I know we wrote our own convolution functions, but this use case
    # is perfectly suited to Numpy's so we're going to use it intstead.
    # "same" mode ensures that the result will be the same size as
    # the original results.
    for i_row in range(result_history.shape[0]):
        smooth_result_history[i_row, :] = np.convolve(
            result_history[i_row, :], window_filter, mode="same")

    for i_snippet in range(n_snippets):
        i_start_snippet = i_snippet * signal_pts_per_snippet + half_window
        i_end_snippet = (i_snippet + 1) * signal_pts_per_snippet + half_window
        t_start_snippet = (i_start_snippet - half_window) / f_samp
        t_end_snippet = (i_end_snippet - half_window) / f_samp
        t_snippet = np.arange(t_start_snippet, t_end_snippet, 1 / f_samp)
        snippet = signal[:2, i_start_snippet: i_end_snippet]

        i_start_result = int(i_snippet * results_pts_per_snippet)
        i_end_result = int((i_snippet + 1) * results_pts_per_snippet)
        t_start_result = i_start_result * pt_spacing / f_samp
        t_end_result = i_end_result * pt_spacing / f_samp
        t_result = np.arange(t_start_result, t_end_result, pt_spacing / f_samp)
        results = smooth_result_history[:, i_start_result: i_end_result]

        ax_left = .1
        ax_width = .85
        fig = plt.figure(figsize=(16 / 2.54, 9 / 2.54))
        ax_signal = fig.add_axes((ax_left, .45, ax_width, .5))
        ax_signal.plot(t_snippet, snippet.transpose(), linewidth=.5)
        ax_signal.plot(
            [t_start_snippet, t_end_snippet],
            [0, 0],
            linewidth=.3,
            color="gray")

        i_labels = np.where(np.logical_and(
            label_locs > i_start_snippet,
            label_locs < i_end_snippet))[0]
        for i_label in i_labels:
            label = labels[int(i_label)]
            label_loc = label_locs[int(i_label)]
            if label in ["N", "L", "/", "V"]:
                t_label = (label_loc - half_window) / f_samp
                ax_signal.text(
                    t_label,
                    -2.2,
                    label_name[label],
                    fontsize=4,
                    horizontalalignment="center",
                    verticalalignment="center",
                    zorder=4,
                )

        ax_signal.tick_params(bottom=False, left=False)
        ax_signal.tick_params(labelbottom=False, labelleft=False)
        ax_signal.spines["top"].set_visible(False)
        ax_signal.spines["bottom"].set_visible(False)
        ax_signal.spines["right"].set_visible(False)
        ax_signal.spines["left"].set_visible(False)
        ax_signal.tick_params(labelsize=6)
        ax_signal.grid(color="gray", linewidth=.3, linestyle=":")
        ax_signal.set_xlim(t_start_snippet, t_end_snippet)
        ax_signal.set_ylim(-2, 2)

        ax_result = fig.add_axes((ax_left, .15, ax_width, .25))
        ax_result.plot(t_result, results[0, :] - .5, linewidth=.3)
        ax_result.plot(t_result, results[1, :] + .5, linewidth=.3)
        ax_result.plot(t_result, results[2, :] + 1.5, linewidth=.3)
        ax_result.plot(t_result, results[3, :] + 2.5, linewidth=.3)
        ax_result.spines["top"].set_visible(False)
        ax_result.spines["bottom"].set_visible(False)
        ax_result.spines["right"].set_visible(False)
        ax_result.spines["left"].set_visible(False)
        ax_result.tick_params(bottom=False, left=False)
        ax_result.tick_params(labelsize=6)
        ax_result.set_xlim(t_start_result, t_end_result)
        ax_result.set_ylim(-.5, 3.5)
        ax_result.set_xlabel("time (s)", fontsize=6)

        # x_formatter = FixedFormatter(["-1e7", "111", "007", "xkcd"])
        # x_locator = FixedLocator([3, 7, 8.8, 12])
        y_formatter = FixedFormatter([
            labels_by_index[0],
            labels_by_index[1],
            labels_by_index[2],
            labels_by_index[3]])
        y_locator = FixedLocator([0, 1, 2, 3])
        # ax.xaxis.set_major_formatter(x_formatter)
        # ax.xaxis.set_major_locator(x_locator)
        ax_result.yaxis.set_major_formatter(y_formatter)
        ax_result.yaxis.set_major_locator(y_locator)

        ax_result.grid(color="gray", linewidth=.3, linestyle=":")
        imgname = f"trace_{patient_id}_{i_snippet + 100}.png"
        fig.savefig(os.path.join(images_dir, imgname), dpi=300)
        plt.close()


def make_ecg_movie(signal, label_locs, labels, patient_id, videos_dir):
    pass
